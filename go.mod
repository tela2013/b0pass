module b0pass

go 1.12

require (
	github.com/gogf/gf v1.9.10
	github.com/xujiajun/nutsdb v0.4.0
	github.com/zserge/lorca v0.1.8
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
)
